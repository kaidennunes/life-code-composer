//	life.c	03/25/2014
//******************************************************************************
//  The Game of Life
//
//  Lab Description:
//
//  The universe of the Game of Life is an infinite two-dimensional orthogonal
//  grid of square cells, each of which is in one of two states, alive or dead.
//  With each new generation, every cell interacts with its eight neighbors,
//  which are the cells horizontally, vertically, or diagonally adjacent
//  according to the following rules:
//
//  1. A live cell stays alive (survives) if it has 2 or 3 live neighbors,
//     otherwise it dies.
//  2. A dead cell comes to life (birth) if it has exactly 3 live neighbors,
//     otherwise it stays dead.
//
//  An initial set of patterns constitutes the seed of the simulation. Each
//  successive generation is created by applying the above rules simultaneously
//  to every cell in the current generation (ie. births and deaths occur
//  simultaneously.)  See http://en.wikipedia.org/wiki/Conway's_Game_of_Life
//
//  Author:    Paul Roper, Brigham Young University
//  Revisions: June 2013   Original code
//             07/12/2013  life_pr, life_cr, life_nr added
//             07/23/2013  generations/seconds added
//             07/29/2013  100 second club check
//             12/12/2013  SWITCHES, display_results, init for port1 & WD
//	           03/24/2014  init_life moved to lifelib.c, 0x80 shift mask
//	                       blinker added, 2x loops
//
//  Built with Code Composer Studio Version: 5.5.0.00090
//******************************************************************************
//
//	This is my work
//	Kaiden Nunes
//	Last Modified: 4/1/2015  @ 10:28pm
//
//******************************************************************************
//******************************************************************************
// includes --------------------------------------------------------------------
#include <stdlib.h>
#include <string.h>
#include "msp430.h"
#include "RBX430-1.h"
#include "RBX430_lcd.h"
#include "life.h"

// global variables ------------------------------------------------------------
extern volatile uint16 WDT_Sec_Cnt;		// WDT second counter
extern volatile uint16 seconds;			// # of seconds
extern volatile uint16 switches;		// debounced switch values

uint8 life[NUM_ROWS][NUM_COLS/8];		// 80 x 80 life grid
uint8 life_pr[NUM_COLS/8];				// previous row
uint8 life_cr[NUM_COLS/8];				// current row
uint8 life_nr[NUM_COLS/8];				// next row
uint8 life_null[NUM_COLS/8];			// null row

//------------------------------------------------------------------------------
//	draw RLE pattern -----------------------------------------------------------
void draw_rle_pattern(int row, int col, const uint8* object)
{
	int x = 0, y = 0, numbOfCells  = 0;
	int stayCol = col;
	// Find x token
	while (*object && (*object++ != 'x'));
	// Find beginning of number
	while(*object && !isdigit(*object)) ++object;
	// Convert to decimal
	while(isdigit(*object)) x = x * 10 + (*object++ - '0');
	// Find y token
	while (*object && (*object++ != 'y'));
	// Find beginning of number
	while(*object && !isdigit(*object)) ++object;
	// Convert to decimal
	while(isdigit(*object)) y = y * 10 + (*object++ - '0');
	row = row + y -1;
	// Find beginning of pattern
	while(*object && (*object++ != '\n'));
	// Dead cell
	while(*object && (*object != '!'))
	{
		numbOfCells = 0;
		while(isdigit(*object)) numbOfCells = numbOfCells * 10 + (*object++ - '0');
		// Dead cell
		if(*object == 'b')
		{
			if(!numbOfCells)
			{
				cell_death(row,col);
				lcd_point(col << 1, row << 1, 6);
				col++;
			}
			for(numbOfCells;numbOfCells;numbOfCells--)
			{
				cell_death(row,col);
				lcd_point(col << 1, row << 1, 6);
				col++;
			}
		}
		// Live cell
		else if(*object == 'o')
		{
			if(!numbOfCells)
			{
				cell_birth(row,col);
				lcd_point(col << 1, row << 1, 7);
				col++;
			}
			for(numbOfCells;numbOfCells;numbOfCells--)
			{
				cell_birth(row,col);
				lcd_point(col << 1, row << 1, 7);
				col++;
			}
		}
		// End of a line of a pattern
		else
		{
			col = stayCol;
			if(!numbOfCells)
			{
				row--;
			}
			for(numbOfCells;numbOfCells;numbOfCells--)
			{
				row--;
			}
		}
		object++;
	}
	row = 0;
	col = 0;
	return;

} // end draw_rle_pattern


//------------------------------------------------------------------------------
// main ------------------------------------------------------------------------
void main(void)
{
	RBX430_init(_16MHZ);				// init board
	ERROR2(lcd_init());					// init LCD
	watchdog_init();					// init watchdog
	port1_init();						// init P1.0-3 switches
	__bis_SR_register(GIE);				// enable interrupts

	while (1)							// new pattern seed
	{
		uint16 generation;				// generation counter
		uint16 row, col;
		uint16 checkCol;
		init_life(NOTHING);				// initialize the game

		memset(life_null, 0, sizeof(life_null));

		lcd_wordImage(life_image, 17, 50, 1);
		lcd_cursor(10, 20);
		printf("\b\tPress Any Key");
		switches = 0;						// clear switches flag
		while (!switches);					// wait for any switch
		init_life(switches);

		WDT_Sec_Cnt = WDT_1SEC_CNT;		// reset WD 1 second counter
		seconds = 0;					// clear second counter
		switches = 0;					// clear switch variable
		generation = 0;					// start generation counter

		while (1)						// next generation
		{
			memcpy(life_pr, life[NUM_ROWS-1], sizeof(life_pr));
			memcpy(life_cr, life[NUM_ROWS-2], sizeof(life_cr));
			memcpy(life_nr, life[NUM_ROWS-3], sizeof(life_nr));
			// for each life row (78 down to 1)
			for (row = NUM_ROWS-2; row; row -= 1)
			{
				if(!memcmp(life_pr, life_null, sizeof(life_pr)) && !memcmp(life_nr, life_null, sizeof(life_nr)) && !memcmp(life_cr, life_null, sizeof(life_cr)))
				{
					memcpy(life_pr, life_cr, sizeof(life_pr));
					memcpy(life_cr, life_nr, sizeof(life_cr));
					memcpy(life_nr, life[row-2], sizeof(life_nr));
					continue;
				}
				// for each life column (78 down to 1)
				col = NUM_COLS-2;
				while(col--)
				{
					if(!life_pr[((col -1)/8)] && !life_cr[((col -1)/8)] && !life_nr[((col -1)/8)] && !life_pr[((col +1)/8)] && !life_cr[((col +1)/8)] && !life_nr[((col +1)/8)]) continue;
					unsigned int numbOfLiveCells = 0;
					checkCol=col-1;
					while(checkCol -1 <= col)
					{
					    if(test_pr_cell(checkCol)) numbOfLiveCells++;
					    if(test_cr_cell(checkCol) && checkCol!=col) numbOfLiveCells++;
					    if(test_nr_cell(checkCol)) numbOfLiveCells++;
					    checkCol++;
					}
					if((numbOfLiveCells < 2 || numbOfLiveCells > 3) && test_cell(row,col))
					{
						cell_death(row,col);
						lcd_point(col << 1, row << 1, 6);
					}
					else if(!(numbOfLiveCells - 3) && !(test_cell(row,col)))
					{
						cell_birth(row,col);
						lcd_point(col << 1, row << 1, 7);
					}
				}
				memcpy(life_pr, life_cr, sizeof(life_pr));
				memcpy(life_cr, life_nr, sizeof(life_cr));
				memcpy(life_nr, life[row-2], sizeof(life_nr));
			}
			// display life generation and generations/second on LCD
			LED_RED_TOGGLE;
			int switching = display_results(++generation);
			if (switching)
			{
				init_life(switching);
				WDT_Sec_Cnt = WDT_1SEC_CNT;		// reset WD 1 second counter
				seconds = 0;					// clear second counter
				switches = 0;					// clear switch variable
				generation = 0;					// start generation counter
			}
		}
	}
} // end main()
